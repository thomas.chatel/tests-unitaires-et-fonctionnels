<?php

namespace App\Http\Controllers;

use App\Models\ToDoList;
use App\Models\User;
use Illuminate\Http\Request;
use Mockery\Exception;

class ToDoListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ToDoList::all();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::findOrFail($request->user_id);
        $result = $user->canCreateToDoList($request->user_id);
        if($result instanceof Exception)
            return response($result->getMessage(), 400);

        $toDoList = new ToDoList([
            'name' => $request->name,
            'user_id' => $request->user_id
        ]);

        $response = $toDoList->isValid();
        if ($response instanceof Exception)
            return response($response->getMessage(), 400);
        return ToDoList::create($toDoList->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return ToDoList::findOrFail($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $toDoList =  ToDoList::find($id);
        $toDoList->name = $request->name;

        $response = $toDoList->isValid();
        if ($response instanceof Exception) {
            return response($response->getMessage(), 400);
        }
        $toDoList->update();
        return $toDoList;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        return ToDoList::destroy($id);
    }
}
