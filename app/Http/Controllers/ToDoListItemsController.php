<?php

namespace App\Http\Controllers;

use App\Models\ToDoList;
use App\Models\ToDoListItems;
use Illuminate\Http\Request;
use Mockery\Exception;

class ToDoListItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ToDoListItems::all();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $toDoList = ToDoList::findOrFail($request->to_do_list_id);
        $result = $toDoList->canAddItems($request->to_do_list_id);
        if($result instanceof Exception)
            return response($result->getMessage(), 400);

        $toDoListItem = new ToDoListItems([
            'name' => $request->name,
            'content' => $request->contenu,
            'to_do_list_id' => $request->to_do_list_id
        ]);

        $response = $toDoListItem->isValid();
        if ($response instanceof Exception)
            return response($response->getMessage(), 400);
        return ToDoListItems::create($toDoListItem->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return ToDoListItems::findOrFail($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $toDoListItem = ToDoListItems::find($id);
        $toDoListItem->name = $request->name;
        $toDoListItem->content = $request->contenu;

        $response = $toDoListItem->isValid();
        if ($response instanceof Exception) {
            return response($response->getMessage(), 400);
        }
        $toDoListItem->update();
        return $toDoListItem;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        return ToDoListItems::destroy($id);
    }
}
