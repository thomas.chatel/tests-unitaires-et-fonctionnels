<?php

namespace App\Models;

use App\Services\ToDoListItemService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Mail\MailServiceProvider;
use Mockery\Exception;

class ToDoList extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id'
    ];

    public function items()
    {
        return $this->hasMany(ToDoListItems::class);
    }

    public function isValid() {
        if (empty($this->name))
            return new Exception('Votre toDoList doit avoir un nom');
        return true;
    }

    public function canAddItems($id) {
        $nbItems = $this->getItemsNumber($id);
        $lastCreatedAt = $this->getLastItemCreatedAt($id);
        if ($nbItems >= 10)
            return new Exception('Vous avez déjà 10 items sur votre toDoList');
        if ( !is_null($lastCreatedAt) && date("Y/m/d H:i:s", strtotime($lastCreatedAt)) > date("Y/m/d H:i:s", strtotime("-30 minutes")) )
            return new Exception ('Le dernier item a été inséré il y a moins de 30 minutees');
        if ($nbItems = 7)

        return true;
    }

    public function getItemsNumber($to_do_list_id) {
        $toDoListItems = ToDoListItems::where('to_do_list_id', $to_do_list_id)->get();
        return $toDoListItems->count();
    }

    public function getLastItemCreatedAt($to_do_list_id) {
        $toDoListItems = ToDoListItems::where('to_do_list_id', $to_do_list_id)
            ->orderBy('created_at')
            ->get();

        $lastItem = $toDoListItems->last();
        if (!is_null($lastItem))
            return $lastItem->created_at;
        return $toDoListItems->last();
    }
}
