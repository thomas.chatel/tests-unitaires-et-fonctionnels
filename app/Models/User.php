<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Mockery\Exception;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'password',
        'age'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isValid(){
        if (empty($this->firstname) || empty($this->lastname))
            return new Exception('Le prénom ou le nom n\'est pas renseigné');
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL))
            return new Exception('Email non valide');
        if (strlen($this->password) < 8 || strlen($this->password) > 40)
            return new Exception('Le mot de passe doit faire entre 8 et 40 caractères');
        if ($this->age < 13)
            return new Exception('L\'utilisateur doit au moins avoir 13ans');
        return true;
    }

    public function canCreateToDoList($user_id) {
        if($this->hasToDoList($user_id))
            return new Exception('Vous avez déjà une toDoList');
        return true;
    }

    public function hasToDoList($user_id) {
        $toDoList = ToDoList::where('user_id', $user_id)->get();
        if ($toDoList->isNotEmpty())
            return true;
        return false;
    }
}
