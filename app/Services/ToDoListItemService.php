<?php

namespace App\Services;

use App\Models\ToDoListItems;

class ToDoListItemService {

    public function isUniqueName(string $name) {
        $result = ToDoListItems::where('name', $name)->get();
        if ($result->isNotEmpty())
            return false;
        return true;
    }

}
