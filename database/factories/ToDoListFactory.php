<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\ToDoList;
use Illuminate\Database\Eloquent\Factories\Factory;

class ToDoListFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ToDoList::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => 'Nom de la Liste',
            'user_id' => User::factory()->create()->id
        ];
    }
}
