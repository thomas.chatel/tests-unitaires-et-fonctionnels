<?php

namespace Database\Factories;

use App\Models\ToDoList;
use App\Models\ToDoListItems;
use Illuminate\Database\Eloquent\Factories\Factory;

class ToDoListItemsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ToDoListItems::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => 'Item Name',
            'content' => "Contenu de l'item",
            'to_do_list_id' => ToDoList::factory()->create()
        ];
    }
}
