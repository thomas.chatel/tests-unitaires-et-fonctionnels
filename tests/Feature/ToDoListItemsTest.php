<?php

namespace Tests\Feature;

use App\Models\ToDoListItems;
use App\Models\ToDoList;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ToDoListItemsTest extends TestCase
{
    use DatabaseTransactions;

    public function testCreate()
    {
        $toDoList = ToDoList::factory()->create();
        $formData = [
            "name" => "List Name",
            "contenu" => "Contenu de l'item 2",
            "to_do_list_id" => $toDoList->id
        ];
        $response = $this->post('/api/items', $formData);

        $response->assertStatus(201);
        $this->assertIsObject(json_decode($response->baseResponse->content()));
    }

    public function testCreateWithIncompleteValues()
    {
        $toDoList = ToDoList::factory()->create();
        $formData = [
            "to_do_list_id" => $toDoList->id
        ];
        $response = $this->post('/api/items', $formData);

        $response->assertStatus(400);
    }

    public function testAddAnOtherItemBefore30min() {
        $toDoList = ToDoList::factory()->create();
        $formData = [
            "name" => "Item 1",
            "contenu" => "Contenu de l'item 1",
            "to_do_list_id" => $toDoList->id
        ];
        $this->post('/api/items', $formData);

        $formData = [
            "name" => "Item 2",
            "contenu" => "Contenu de l'item 2",
            "to_do_list_id" => $toDoList->id
        ];

        $response = $this->post('/api/items', $formData);
        $response->assertStatus(400);
        $this->assertEquals('Le dernier item a été inséré il y a moins de 30 minutees', $response->baseResponse->content());
    }

    public function testAddEleventhItem(){
        $toDoList = ToDoList::factory()->create();

        for($i=0; $i<10; $i++){
            ToDoListItems::create([
                "name" => "Item ". $i,
                "content" => "Contenu de l'item ". $i,
                "to_do_list_id" => $toDoList->id
            ]);
        }

        $formData = [
            "name" => "Item 11",
            "contenu" => "Contenu de l'item 11",
            "to_do_list_id" => $toDoList->id
        ];
        $response = $this->post('/api/items', $formData);

        $response->assertStatus(400);
        $this->assertEquals('Vous avez déjà 10 items sur votre toDoList', $response->baseResponse->content());
    }

    public function testGetAll()
    {
        $response = $this->get('/api/items');

        $response->assertStatus(200);
        $this->assertIsArray(json_decode($response->baseResponse->content()));
    }

    public function testGetOne()
    {
        $toDoListItem = ToDoListItems::factory()->create();
        $response = $this->get('/api/items/' . $toDoListItem->id);

        $response->assertStatus(200);
        $this->assertIsObject(json_decode($response->baseResponse->content()));
    }

    public function testGetOneThatDoNotExist()
    {
        $response = $this->get('/api/items/17000');
        $response->assertStatus(404);
    }

    public function testUpdate()
    {
        $toDoListItem = ToDoListItems::factory()->create();
        $updateData = [
            "name" => "Nom de l'item v2",
            "contenu" => "Contenu de l'item v2"
        ];
        $response = $this->put('/api/items/' . $toDoListItem->id, $updateData);
        $response->assertStatus(200);    }

    public function testUpdateWithIncorrectValues()
    {
        $toDoListItem = ToDoListItems::factory()->create();
        $updateData = [];
        $response = $this->put('/api/items/' . $toDoListItem->id, $updateData);
        $response->assertStatus(400);
    }

    public function testDelete()
    {
        $toDoListItem = ToDoListItems::factory()->create();
        $response = $this->delete('/api/items/' . $toDoListItem->id);
        $response->assertStatus(200);
    }

}
