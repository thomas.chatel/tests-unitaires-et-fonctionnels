<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\ToDoList;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ToDoListsTest extends TestCase
{
    use DatabaseTransactions;

    public function testCreate()
    {
        $user = User::factory()->create();
        $formData = [
            "name" => "List Name",
            "user_id" => $user->id
        ];
        $response = $this->post('/api/lists', $formData);

        $response->assertStatus(201);
        $this->assertIsObject(json_decode($response->baseResponse->content()));
    }

    public function testCreateWithIncompleteValues()
    {
        $user = User::factory()->create();
        $formData = [
            "user_id" => $user->id
        ];
        $response = $this->post('/api/lists', $formData);

        $response->assertStatus(400);
    }

    public function testCreateASecondToDoList() {
        $user = User::factory()->create();

        $formData = [
            "name" => "List 1",
            "user_id" => $user->id
        ];
        $this->post('/api/lists', $formData);

        $formData = [
            "name" => "List 1",
            "user_id" => $user->id
        ];
        $response = $this->post('/api/lists', $formData);

        $response->assertStatus(400);
        $this->assertEquals('Vous avez déjà une toDoList', $response->baseResponse->content());
    }

    public function testGetAll()
    {
        $response = $this->get('/api/lists');

        $response->assertStatus(200);
        $this->assertIsArray(json_decode($response->baseResponse->content()));
    }

    public function testGetOne()
    {
        $toDoList = ToDoList::factory()->create();
        $response = $this->get('/api/lists/' . $toDoList->id);

        $response->assertStatus(200);
        $this->assertIsObject(json_decode($response->baseResponse->content()));
    }

    public function testGetOneThatDoNotExist()
    {
        $response = $this->get('/api/lists/17000');
        $response->assertStatus(404);
    }

    public function testUpdate()
    {
        $toDoList = ToDoList::factory()->create();
        $updateData = [
            "name" => "Nom de la liste v2"
        ];
        $response = $this->put('/api/lists/' . $toDoList->id, $updateData);
        $response->assertStatus(200);
    }

    public function testUpdateWithIncorrectValues()
    {
        $toDoList = ToDoList::factory()->create();
        $updateData = [];
        $response = $this->put('/api/lists/' . $toDoList->id, $updateData);
        $response->assertStatus(400);
    }

    public function testDelete()
    {
        $toDoList = ToDoList::factory()->create();
        $response = $this->delete('/api/lists/' . $toDoList->id);
        $response->assertStatus(200);
    }
}
