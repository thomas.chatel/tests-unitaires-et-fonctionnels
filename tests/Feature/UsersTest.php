<?php

namespace Tests\Feature;

use App\Http\Controllers\UserController;
use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UsersTest extends TestCase
{

    public function testCreate()
    {
        $formData = [
            "firstname" => "Thomas",
            "lastname" => "Chatel",
            "email" => "thomas.chatel@test.fr",
            "age" => 22,
            "password" => "testtest"
        ];
        $response = $this->post('/api/users', $formData);

        $response->assertStatus(201);
        $this->assertIsObject(json_decode($response->baseResponse->content()));
    }

    public function testCreateWithIncompleteValues()
    {
        $formData = [
            "firstname" => "Thomas",
            "email" => "thomas.chatel@test.fr",
        ];
        $response = $this->post('/api/users', $formData);

        $response->assertStatus(400);
    }

    public function testCreateWithIncorrectValues()
    {
        $formData = [
            "firstname" => "Thomas",
            "lastname" => "Chatel",
            "email" => "thomas.chatel",
            "age" => 12,
            "password" => "test"
        ];
        $response = $this->post('/api/users', $formData);

        $response->assertStatus(400);
    }

    public function testGetAll()
    {
        $response = $this->get('/api/users');

        $response->assertStatus(200);
        $this->assertIsArray(json_decode($response->baseResponse->content()));
    }

    public function testGetOne()
    {
        $user = User::factory()->create();
        $response = $this->get('/api/users/' . $user->id);

        $response->assertStatus(200);
        $this->assertIsObject(json_decode($response->baseResponse->content()));
    }

    public function testGetOneThatDoNotExist()
    {
        $response = $this->get('/api/users/17000');
        $response->assertStatus(404);
    }

    public function testUpdate()
    {
        $user = User::factory()->create();
        $updateData = [
            "firstname" => "Jacques",
            "lastname" => "CAI",
            "email" => "jacques.cai@test.fr",
            "age" => 20,
            "password" => "testtest"
        ];
        $response = $this->put('/api/users/' . $user->id, $updateData);
        $response->assertStatus(200);    }

    public function testUpdateWithIncorrectValues()
    {
        $user = User::factory()->create();
        $updateData = [
            "firstname" => "Thomas",
            "lastname" => "Chatel",
            "email" => "thomas.chatel",
            "age" => 12,
            "password" => "test"
        ];
        $response = $this->put('/api/users/' . $user->id, $updateData);
        $response->assertStatus(400);
    }

    public function testDelete()
    {
        $user = User::factory()->create();
        $response = $this->delete('/api/users/' . $user->id);
        $response->assertStatus(200);
    }
}
